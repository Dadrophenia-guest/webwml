#use wml::debian::translation-check translation="51763aba27b9d66db99fcc27f02ef7384e2ec8c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Les CVE suivants ont été signalés envers jackson-databind.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10672">CVE-2020-10672</a>

<p>jackson-databind 2.x avant 2.9.10.4 de FasterXML gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, associée à
org.apache.aries.transaction.jms.internal.XaPooledConnectionFactory
(c'est-à-dire aries.transaction.jms).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10673">CVE-2020-10673</a>

<p>jackson-databind 2.x avant 2.9.10.4 de FasterXML gère incorrectement
l’interaction entre les gadgets de sérialisation et la saisie, associée à
com.caucho.config.types.ResourceRef (c'est-à-dire caucho-quercus).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.4.2-2+deb8u13.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jackson-databind.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2153.data"
# $Id: $
