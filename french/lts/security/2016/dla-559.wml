#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le démon du protocole
NTP (« Network Time Protocol ») et des utilitaires :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7974">CVE-2015-7974</a>

<p>Matt Street a découvert qu'une validation de clé insuffisante permet des
attaques par usurpation d'identité entre pairs authentifiés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7977">CVE-2015-7977</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2015-7978">CVE-2015-7978</a>

<p>Stephen Gray a découvert qu'un déréférencement de pointeur NULL et un
dépassement de tampon dans le traitement des commandes <q>ntpdc reslist</q>
peuvent avoir pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7979">CVE-2015-7979</a>

<p>Aanchal Malhotra a découvert que si NTP est configuré en mode diffusion,
un attaquant peut envoyer des paquets d'authentification mal formés qui
brisent l'association avec le serveur d'autres clients de diffusion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8138">CVE-2015-8138</a>

<p>Matthew van Gundy et Jonathan Gardner ont découvert que l'absence de
validation des estampilles temporelles d'origine dans les clients ntpd peut
avoir pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8158">CVE-2015-8158</a>

<p>Jonathan Gardner a découvert que l'absence de vérification des entrées
dans ntpq peut avoir pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1547">CVE-2016-1547</a>

<p>Stephen Gray et Matthew van Gundy ont découvert qu'un traitement
incorrect de paquets crypto-NAK peut avoir pour conséquence un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1548">CVE-2016-1548</a>

<p>Jonathan Gardner et Miroslav Lichvar ont découvert que des clients ntpd
pourraient être contraints de passer du mode basique client/serveur au mode
entrelacé symétrique, empêchant la synchronisation de l'heure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1550">CVE-2016-1550</a>

<p>Matthew van Gundy, Stephen Gray et Loganaden Velvindron ont découvert
que des fuites de temporisation dans le code d'authentification de paquet
pourraient avoir pour conséquence la récupération d'un condensé de message.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2516">CVE-2016-2516</a>

<p>Yihan Lian a découvert que des adresses IP dupliquées dans des
directives <q>unconfig</q> déclenchent une assertion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2518">CVE-2016-2518</a>

<p>Yihan Lian a découvert qu'un accès mémoire hors limites pourrait
éventuellement planter ntpd.</p>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1:4.2.6.p5+dfsg-2+deb7u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ntp.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-559.data"
# $Id: $
