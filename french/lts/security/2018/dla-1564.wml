#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que l’analyseur de Mono «·string-to-double·» pouvait
planter avec une entrée spécialement contrefaite. Cela pourrait conduire à
l’exécution de code arbitraire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1002208">CVE-2018-1002208</a>

<p>Mono intègre la bibliothèque sharplibzip qui est vulnérable à une
traversée de répertoires, permettant à des attaquants d’écrire des fichiers
arbitraires à l'aide d'un ../ (point point barre oblique) dans une entrée
d’archive Zip, mal gérée durant l’extraction. Cette vulnérabilité est aussi
connue comme «·Zip-Slip·».</p>

<p>Les développeurs de Mono envisagent de retirer entièrement sharplibzip des
sources et n’envisagent pas de corriger ce problème. Il est par conséquent
recommandé de récupérer la dernière version de sharplibzip en utilisant le
gestionnaire de paquets nuget. La version intégrée ne devrait pas être utilisée
avec des fichiers zip non sûrs.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans la version 3.2.8+dfsg-10+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mono.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1564.data"
# $Id: $
