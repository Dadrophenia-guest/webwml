#use wml::debian::translation-check translation="3deacdd52e79bbcb35e11a02af6d88e8213ad196" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le serveur HTTP Apache.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0217">CVE-2019-0217</a>

<p>Une situation de compétition dans mod_auth_digest lorsqu'il est exécuté
dans un serveur multitâche (threaded server) pourrait permettre à un
utilisateur avec une identité valable de s'identifier avec un autre nom
d'utilisateur, en contournant les restrictions de contrôle d'accès
configurées. Ce problème a été découvert par Simon Kappel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0220">CVE-2019-0220</a>

<p>Bernhard Lorenz de Alpha Strike Labs GmbH a signalé que la normalisation
d'URL était gérée de manière incohérente. Quand le composant chemin d'une
URL de requête contient plusieurs barres obliques consécutives (« / »),
des directives telles que LocationMatch et RewriteRule doivent les compter
comme dupliquées dans les expressions rationnelles tandis que d'autres
aspects du traitement des serveurs les écraseront de façon implicite.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.4.10-10+deb8u14.</p>
<p>Nous vous recommandons de mettre à jour vos paquets apache2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1748.data"
# $Id: $
