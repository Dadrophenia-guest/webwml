#use wml::debian::translation-check translation="3b0824177c1b8047b44da4cff879abac26ac9fe7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il existait une vulnérabilité de code arbitraire à distance dans
commons-beanutils, un ensemble d’utilitaires pour la manipulation de code
JavaBeans.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10086">CVE-2019-10086</a>

<p>Dans Apache Commons Beanutils 1.9.2, une classe spéciale BeanIntrospector
était ajoutée qui permettait de supprimer la possibilité pour un attaquant
d’accéder au chargeur de classe à l’aide d’une propriété de classe disponible
pour tous les objets Java. Nous n'utilisions cela en aucune façon à cause d’une
caractéristique par défaut de PropertyUtilsBean.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.9.2-1+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets commons-beanutils.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1896.data"
# $Id: $
