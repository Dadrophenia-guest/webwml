#use wml::debian::translation-check translation="1b8cc90ab6815964cced62616d992f4c73343619" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes de sécurité ont été corrigés dans libav, une bibliothèque
multimédia pour le traitement de fichiers audio et vidéo.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17127">CVE-2017-17127</a>

<p>La fonction vc1_decode_frame dans libavcodec/vc1dec.c permet à des attaquants
distants de provoquer un déni de service (déréférencement de pointeur NULL et
plantage d'application) à l'aide d'un fichier contrefait.<br />
<a href="https://security-tracker.debian.org/tracker/CVE-2018-19130">CVE-2018-19130</a>
est une reproduction de cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18245">CVE-2017-18245</a>

<p>La fonction mpc8_probe dans libavformat/mpc8.c permet à des attaquants
distants de provoquer un déni de service (lecture hors limites de tampon basé
sur le tas) à l'aide d'un fichier audio contrefait sur les systèmes 32 bits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19128">CVE-2018-19128</a>

<p>Une lecture hors limites de tampon basé sur le tas dans decode_frame dans
libavcodec/lcldec.c permet à un attaquant de provoquer un déni de service à
l'aide d'un fichier avi contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14443">CVE-2019-14443</a>

<p>Une division par zéro dans range_decode_culshift dans libavcodec/apedec.c
permet à des attaquants distants de provoquer un déni de service (plantage
d'application), comme montré par avconv.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17542">CVE-2019-17542</a>

<p>Dépassement de tampon basé sur le tas dans vqa_decode_chunk à cause d’un
accès hors tableau dans vqa_decode_init dans libavcodec/vqavideo.c.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 6:11.12-1~deb8u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2021.data"
# $Id: $
