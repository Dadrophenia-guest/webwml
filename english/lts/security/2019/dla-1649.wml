<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Christophe Fergeau discovered an out-of-bounds read vulnerability in
spice, a SPICE protocol client and server library, which might result in
denial of service (spice server crash), or possibly, execution of
arbitrary code.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.12.5-1+deb8u7.</p>

<p>We recommend that you upgrade your spice packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1649.data"
# $Id: $
