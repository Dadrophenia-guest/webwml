<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were two vulnerabilities in the rsyslog
system/kernel logging daemon in the parsers for AIX and Cisco log
messages respectfully.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17041">CVE-2019-17041</a>

    <p>An issue was discovered in Rsyslog v8.1908.0.
    contrib/pmaixforwardedfrom/pmaixforwardedfrom.c has a heap overflow in the
    parser for AIX log messages. The parser tries to locate a log message
    delimiter (in this case, a space or a colon) but fails to account for
    strings that do not satisfy this constraint. If the string does not match,
    then the variable lenMsg will reach the value zero and will skip the sanity
    check that detects invalid log messages. The message will then be
    considered valid, and the parser will eat up the nonexistent colon
    delimiter. In doing so, it will decrement lenMsg, a signed integer, whose
    value was zero and now becomes minus one. The following step in the parser
    is to shift left the contents of the message. To do this, it will call
    memmove with the right pointers to the target and destination strings, but
    the lenMsg will now be interpreted as a huge value, causing a heap
    overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17042">CVE-2019-17042</a>

    <p>An issue was discovered in Rsyslog v8.1908.0.
    contrib/pmcisconames/pmcisconames.c has a heap overflow in the parser for
    Cisco log messages. The parser tries to locate a log message delimiter (in
    this case, a space or a colon), but fails to account for strings that do
    not satisfy this constraint. If the string does not match, then the
    variable lenMsg will reach the value zero and will skip the sanity check
    that detects invalid log messages. The message will then be considered
    valid, and the parser will eat up the nonexistent colon delimiter. In doing
    so, it will decrement lenMsg, a signed integer, whose value was zero and
    now becomes minus one. The following step in the parser is to shift left
    the contents of the message. To do this, it will call memmove with the
    right pointers to the target and destination strings, but the lenMsg will
    now be interpreted as a huge value, causing a heap overflow.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
8.4.2-1+deb8u3.</p>

<p>We recommend that you upgrade your rsyslog packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1952.data"
# $Id: $
