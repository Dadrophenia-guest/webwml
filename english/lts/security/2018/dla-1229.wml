<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there were two vulnerabilities in the imagemagick
image manipulation program:</p>

<ul>
  <li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000445">CVE-2017-1000445</a>
  <p>A null pointer dereference in the MagickCore
  component which could lead to denial of service.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000476">CVE-2017-1000476</a>
  <p>A potential denial of service attack via CPU
  exhaustion.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in imagemagick version
8:6.7.7.10-5+deb7u20.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1229.data"
# $Id: $
