<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Phpmyadmin, a web administration tool for MySQL, had several
Cross Site Scripting (XSS) vulnerabilities were reported.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5731">CVE-2016-5731</a>

    <p>With a specially crafted request, it is possible to trigger
    an XSS attack through the example OpenID authentication script.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5733">CVE-2016-5733</a>

    <p>Several XSS vulnerabilities were found with the Transformation
    feature.
    Also a vulnerability was reported allowing a specifically
    configured MySQL server to execute an XSS attack.
    This particular attack requires configuring the MySQL server
    log_bin directive with the payload.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5739">CVE-2016-5739</a>

    <p>A vulnerability was reported where a specially crafted
    Transformation could be used to leak information including
    the authentication token. This could be used to direct a
    CSRF attack against a user.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4:3.4.11.1-2+deb7u5.</p>

<p>We recommend that you upgrade your phpmyadmin packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-551.data"
# $Id: $
