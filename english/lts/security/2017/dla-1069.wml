<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Tenshi creates a tenshi.pid file after dropping privileges to a non-root
account, which might allow local users to kill arbitrary processes by
leveraging access to this non-root account for tenshi.pid modification before a
root script executes a "kill `cat /pathname/tenshi.pid`" command.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.13-2+deb7u1.</p>

<p>We recommend that you upgrade your tenshi packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1069.data"
# $Id: $
