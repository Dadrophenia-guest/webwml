<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that poppler, a PDF rendering library, was affected
by several denial-of-service (application crash), null pointer
dereferences and heap-based buffer over-read bugs:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14975">CVE-2017-14975</a>

  <p>The FoFiType1C::convertToType0 function in FoFiType1C.cc
  has a NULL pointer dereference vulnerability because a data structure
  is not initialized, which allows an attacker to launch a denial of
  service attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14976">CVE-2017-14976</a>

  <p>The FoFiType1C::convertToType0 function in FoFiType1C.cc
  has a heap-based buffer over-read vulnerability if an out-of-bounds
  font dictionary index is encountered, which allows an attacker to
  launch a denial of service attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14977">CVE-2017-14977</a>

  <p>The FoFiTrueType::getCFFBlock function in FoFiTrueType.cc
  has a NULL pointer dereference vulnerability due to lack of validation
  of a table pointer, which allows an attacker to launch a denial of
  service attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15565">CVE-2017-15565</a>

  <p>NULL Pointer Dereference exists in the GfxImageColorMap::getGrayLine()
  function in GfxState.cc via a crafted PDF document.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.18.4-6+deb7u4.</p>

<p>We recommend that you upgrade your poppler packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1177.data"
# $Id: $
