# Jorge Barreiro <yortx.barry@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2012-11-20 21:02+0100\n"
"Last-Translator: Jorge Barreiro <yortx.barry@gmail.com>\n"
"Language-Team: Galician <proxecto@trasno.net>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../english/CD/vendors/vendors.CD.def:14
msgid "Vendor"
msgstr "Vendedor"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr "Permite contribucións"

#: ../../english/CD/vendors/vendors.CD.def:16
#, fuzzy
msgid "CD/DVD/BD/USB"
msgstr "CD/DVD"

#: ../../english/CD/vendors/vendors.CD.def:17
msgid "Architectures"
msgstr "Arquitecturas"

#: ../../english/CD/vendors/vendors.CD.def:18
msgid "Ship International"
msgstr "Envíos internacionais"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr "Contacto"

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
msgid "Vendor Home"
msgstr "Páxina principal do vendedor"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr "páxina"

#: ../../english/CD/vendors/vendors.CD.def:71
msgid "email"
msgstr "correo"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "en Europa"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "A algunhas áreas"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "fonte"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "e"

#~ msgid "Architectures:"
#~ msgstr "Arquitecturas:"

#~ msgid "DVD Type:"
#~ msgstr "Tipo de DVD"

#~ msgid "CD Type:"
#~ msgstr "Tipo de CD:"

#~ msgid "email:"
#~ msgstr "correo:"

#~ msgid "Ship International:"
#~ msgstr "Envíos internacionais:"

#~ msgid "Country:"
#~ msgstr "País:"

#~ msgid "Allows Contribution to Debian:"
#~ msgstr "Permite contribucións a Debian:"

#~ msgid "URL for Debian Page:"
#~ msgstr "URL da páxina de Debian:"

#~ msgid "Vendor:"
#~ msgstr "Vendedor:"
